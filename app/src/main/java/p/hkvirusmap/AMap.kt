package p.hkvirusmap

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.a_maps.*
import p.common.LogDog
import p.common.MyPref
import p.common.Util
import p.data.DB
import p.data.TableDef
import p.data.TblCase
import p.data.TblCaseMap
import java.util.*

class AMap : AppCompatActivity(), OnMapReadyCallback, LocationListener,
	GoogleMap.OnMarkerClickListener {

	companion object {
		private val TAG = "MapsActivity"
		val REQUEST_CASENO = 201
		val ARG_SEARCH_CASENO = "ARG_SEARCH_CASENO"
		val ARG_SEARCH_DATE = "ARG_SEARCH_DATE"
	}

	private val DEFAULT_ZOOM = 17f
	private val MAX_ZOOM = 19f
	private val MIN_ZOOM = 11f
	private val ALERT_DISTANCE = 200

	private var _map: GoogleMap? = null
	private var _locationManager : LocationManager? = null
	private var _mapicon : BitmapDescriptor? = null
	private var _mapicon_yellow : BitmapDescriptor? = null
	private var _search_caseNo = 0
	private var _search_date = ""
	private var _caseNo_yellow_max = 0

	private val _fetchListener : (Int)->Unit = {count->
		Util.showToast(getString(R.string.map_new_cases, count))
		iv_logo.clearAnimation()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.a_maps)
		supportActionBar?.hide()
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		val mapFragment = supportFragmentManager
			.findFragmentById(R.id.map) as SupportMapFragment
		mapFragment.getMapAsync(this)
		_locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
		_mapicon = BitmapDescriptorFactory.fromResource(R.mipmap.mapicon)
		_mapicon_yellow = BitmapDescriptorFactory.fromResource(R.mipmap.mapicon_yellow)
		iv_logo.setOnClickListener { showMenu() }
		val now = System.currentTimeMillis()
		val lastUpdate = MyPref.getPref(MyPref.PREF_LAST_UPDATE, 0L)
		if (now - lastUpdate > Util.SIX_HOUR)
			startRefresh()
		ib_clear.setOnClickListener {
			_search_caseNo = 0
			_search_date = ""
			if (_map != null) {
				tv_title.setText(getString(R.string.app_name))
				tv_title.visibility = View.VISIBLE
				ib_clear.visibility = View.INVISIBLE
				_map!!.clear()
				_marks.clear()
				_mapMoved()
			}
		}
	}

	private fun startRefresh() {
		MyPref.setPref(MyPref.PREF_LAST_UPDATE, System.currentTimeMillis())
		Fetcher.doIt(_fetchListener)
		val anim = AnimationUtils.loadAnimation(this, R.anim.myrotate)
		anim.interpolator = LinearInterpolator()
		iv_logo.startAnimation(anim)
	}

	override fun onStart() {
		super.onStart()
		try {
			_locationManager?.requestSingleUpdate(
				LocationManager.GPS_PROVIDER,
				this, null)
		} catch (e:SecurityException) {}
		val yellow_day = System.currentTimeMillis() - Util.MILLISECONDS_PER_DAY * 14
		val yellow_day_string = Util.getDateString(yellow_day)
		_caseNo_yellow_max = DB.getIntMinus1(
			TblCase,
			"MAX(${TblCase._caseNo})",
			"${TblCase._confirmDate}='$yellow_day_string'")
	}

	// MAP related

	private val _marks = HashSet<Long>()

	private var _homeLocation : Location? = null

	private var _checkAlert = false

	private val _mapMoved :() -> Unit = {
		val region = _map!!.projection.visibleRegion
		val left = region.latLngBounds.southwest.longitude
		val bottom = region.latLngBounds.southwest.latitude
		val top = region.latLngBounds.northeast.latitude
		val right = region.latLngBounds.northeast.longitude
		var where = """${TblCaseMap._latitude} > $bottom AND
				${TblCaseMap._latitude} < $top AND
				${TblCaseMap._longitude} < $right AND
				${TblCaseMap._longitude} > $left """
		if (_search_caseNo != 0)
			where += " AND ${TblCaseMap._caseNo}=$_search_caseNo "
		if (_search_date != "") {
			where += " AND ${TblCaseMap._caseNo} IN (SELECT ${TblCase._caseNo} FROM ${TblCase._T} WHere ${TblCase._confirmDate}='$_search_date') "
		}
		val cursor = DB.query(TblCaseMap, where)
		val indexId = cursor.getColumnIndex(TableDef._ID)
		val indexLat = cursor.getColumnIndex(TblCaseMap._latitude)
		val indexLong = cursor.getColumnIndex(TblCaseMap._longitude)
		val indexCaseNo = cursor.getColumnIndex(TblCaseMap._caseNo)
		var minDistance = Int.MAX_VALUE
		while (cursor.moveToNext()) {
			val id = cursor.getLong(indexId)
			if (_marks.contains(id))
				continue
			_marks.add(id)
			val position = LatLng(cursor.getDouble(indexLat), cursor.getDouble(indexLong))
			val caseNo = cursor.getInt(indexCaseNo)
			val marker = MarkerOptions().position(position).title("$caseNo")
			if (caseNo > _caseNo_yellow_max)
				marker.icon(_mapicon)
			else
				marker.icon(_mapicon_yellow)
			_map!!.addMarker(marker)
			if (_checkAlert && _homeLocation != null) {
				val location = Location("")
				location.latitude = position.latitude
				location.longitude = position.longitude
				val distance = location.distanceTo(_homeLocation).toInt()
				if (distance < minDistance)
					minDistance = distance
			}
		}
		cursor.close()
		if (_checkAlert) {
			_checkAlert = false
			if (minDistance < ALERT_DISTANCE) {
				val s = getString(R.string.map_alert, minDistance)
				Util.showAlert(this, s, {}, null)
			}
		}
	}

	/**
	 * Manipulates the map once available.
	 * This callback is triggered when the map is ready to be used.
	 * This is where we can add markers or lines, add listeners or move the camera. In this case,
	 * we just add a marker near Sydney, Australia.
	 * If Google Play services is not installed on the device, the user will be prompted to install
	 * it inside the SupportMapFragment. This method will only be triggered once the user has
	 * installed Google Play services and returned to the app.
	 */
	override fun onMapReady(googleMap: GoogleMap) {
		_map = googleMap
		_map!!.setMaxZoomPreference(MAX_ZOOM)
		_map!!.setMinZoomPreference(MIN_ZOOM)
		// default to HK government
		val hk = LatLng(22.28, 114.166)
		_map!!.moveCamera(CameraUpdateFactory.newLatLngZoom(hk, DEFAULT_ZOOM))
		_map!!.setOnCameraMoveListener(_mapMoved)
		_map!!.setOnMarkerClickListener(this)
		_map!!.setOnMapClickListener {	tv_title.visibility = View.INVISIBLE }
	}

	override fun onLocationChanged(location: Location) {
		if (_homeLocation != null) {
			LogDog.i(TAG,"onLocationChanged is called again, skip")
			return
		}
		// check alerts
		_checkAlert = true
		_homeLocation = location
		// move to home location
		val latLng = LatLng(location.latitude, location.longitude)
		val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM)
		_map?.animateCamera(cameraUpdate)
		_map?.addMarker(MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.mipmap.home)).position(latLng))
		_locationManager?.removeUpdates(this)
	}

	override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {	}

	override fun onProviderEnabled(provider: String?) {	}

	override fun onProviderDisabled(provider: String?) { }

	override fun onMarkerClick(marker : Marker): Boolean {
		try {
			val caseNo = marker.title.toInt()
			DCase(this, caseNo).show()
		} catch (e:NumberFormatException) { }
		return true
	}

	// menu related

	private val _menuListener : (Int)->Unit = { menuId ->
		when (menuId) {
			R.string.map_menu_search -> startActivityForResult(Intent(this, ASearch::class.java), REQUEST_CASENO)
			R.string.map_menu_refresh -> startRefresh()
			R.string.map_menu_delete_refresh -> {
				DB.sql().delete(TblCaseMap._T, "", null)
				DB.sql().delete(TblCase._T, "", null)
				startRefresh()
			}
			R.string.map_menu_disclaimer -> Util.showAlert(this, R.string.disclaimer, {}, null)
			R.string.map_menu_rate -> {
				val s: String = PApp._instance!!.packageName
				try {
					startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$s")))
				} catch (anfe: ActivityNotFoundException) {
					startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$s")))
				}
			}
		}
	}

	private fun showMenu() {
		val menus = ArrayList<Pair<Int, Int>>()
		menus.add(Pair(android.R.drawable.ic_menu_month, R.string.map_menu_search))
		menus.add(Pair(android.R.drawable.ic_menu_rotate, R.string.map_menu_refresh))
		menus.add(Pair(android.R.drawable.ic_menu_delete, R.string.map_menu_delete_refresh))
//		menus.add(Pair(android.R.drawable.ic_media_play, R.string.map_menu_trace))
		menus.add(Pair(android.R.drawable.ic_menu_info_details, R.string.map_menu_disclaimer))
		menus.add(Pair(android.R.drawable.ic_menu_edit, R.string.map_menu_rate))
		DMenu(this, menus, _menuListener).show()
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == REQUEST_CASENO && data != null) {
			_search_caseNo = data.getIntExtra(ARG_SEARCH_CASENO, 0)
			_search_date = data.getStringExtra(ARG_SEARCH_DATE)?:""
			if (_map != null) {
				if (_search_caseNo != 0)
					tv_title.setText(getString(R.string.map_case, _search_caseNo))
				else if (_search_date != "")
					tv_title.setText(_search_date)
				tv_title.visibility = View.VISIBLE
				ib_clear.visibility = View.VISIBLE
				_map!!.clear()
				_marks.clear()
				// move to the first abode
				if (_search_caseNo != 0) {
					val q = DB.query(TblCaseMap, "${TblCaseMap._caseNo}=$_search_caseNo")
					if (q.moveToFirst()) {
						val lat = q.getDouble(q.getColumnIndex(TblCaseMap._latitude))
						val lon = q.getDouble(q.getColumnIndex(TblCaseMap._longitude))
						val latLng = LatLng(lat, lon)
						val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM)
						_map!!.animateCamera(cameraUpdate)
					}
					q.close()
				}
			}
		}
	}

}
