package p.hkvirusmap

import android.content.Intent
import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.a_search.*
import p.common.MyPref
import p.common.Util
import p.data.DB
import p.data.TblCase

class ASearch : AppCompatActivity() {

	companion object {
		val TAG = "ASearch"
	}

	private var _searchDate = ""
	private var _caseNo_yellow_max = 0

	private val _listener : (String)->Unit = { s ->
		tv_title.setText(s)
		searchDate(s)
		MyPref.setPref(MyPref.PREF_LAST_QUERY_DATE, s)
		rv.adapter?.notifyDataSetChanged()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.a_search)
		supportActionBar?.hide()
		rv.layoutManager = LinearLayoutManager(this)
		ib_date.setOnClickListener {
			DDate(this, tv_title.text.toString(), _listener).show()
		}
		btn_search_all.setOnClickListener(_searchAllClicker)
	}

	override fun onStart() {
		super.onStart()
		val sdate = MyPref.getPref(MyPref.PREF_LAST_QUERY_DATE, "")
		if (sdate != "")
			tv_title.setText(sdate)
		searchDate(tv_title.text.toString())
		rv.adapter = AdpCaseNo()
		rv.adapter?.notifyDataSetChanged()
		val yellow_day = System.currentTimeMillis() - Util.MILLISECONDS_PER_DAY * 14
		val yellow_day_string = Util.getDateString(yellow_day)
		_caseNo_yellow_max = DB.getIntMinus1(
			TblCase,
			"MAX(${TblCase._caseNo})",
			"${TblCase._confirmDate}='$yellow_day_string'")
	}

	override fun onStop() {
		super.onStop()
		_cursor?.close()
	}

	private var _cursor : Cursor? = null
	private var _indexCaseNo = 0
	private var _indexGender = 0
	private var _indexAge = 0
	private var _indexClassification = 0

	private fun searchDate(date:String) {
		_searchDate = date
		_cursor?.close()
		_cursor = DB.query(TblCase, "${TblCase._confirmDate}='$date'", TblCase._caseNo)
		_indexCaseNo = _cursor!!.getColumnIndex(TblCase._caseNo)
		_indexAge = _cursor!!.getColumnIndex(TblCase._age)
		_indexGender = _cursor!!.getColumnIndex(TblCase._gender)
		_indexClassification = _cursor!!.getColumnIndex(TblCase._classification)
		tv_count.text = _cursor!!.count.toString()
	}

	private val _caseClicker : (View)->Unit = { v ->
		val caseNo = v.tag as Int
		DCase(this, caseNo).show()
	}

	private val _mapClicker : (View)->Unit = { v ->
		val caseNo = v.tag as Int
		val intent = Intent()
		intent.putExtra(AMap.ARG_SEARCH_CASENO, caseNo)
		intent.putExtra(AMap.ARG_SEARCH_DATE, "")
		setResult(AMap.REQUEST_CASENO, intent)
		finish()
	}

	private val _searchAllClicker : (View)->Unit = { _ ->
		val intent = Intent()
		intent.putExtra(AMap.ARG_SEARCH_CASENO, 0)
		intent.putExtra(AMap.ARG_SEARCH_DATE, _searchDate)
		setResult(AMap.REQUEST_CASENO, intent)
		finish()
	}

	private inner class VHCaseNo(view:View) : RecyclerView.ViewHolder(view) {
		val _tv_caseno = view.findViewById<TextView>(R.id.tv_caseno)
		val _tv_age = view.findViewById<TextView>(R.id.tv_age)
		val _tv_gender = view.findViewById<TextView>(R.id.tv_gender)
		val _tv_classification = view.findViewById<TextView>(R.id.tv_classification)
		val _ib_map = view.findViewById<ImageButton>(R.id.ib_map)
		init {
			_tv_caseno.setOnClickListener(_caseClicker)
			_ib_map.setOnClickListener(_mapClicker)
		}
	}

	private inner class AdpCaseNo : RecyclerView.Adapter<VHCaseNo>() {

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHCaseNo {
			return VHCaseNo(layoutInflater.inflate(R.layout.r_case, parent, false))
		}

		override fun getItemCount(): Int = if (_cursor == null) 0 else _cursor!!.count

		override fun onBindViewHolder(holder: VHCaseNo, position: Int) {
			_cursor!!.moveToPosition(position)
			val caseNo = _cursor!!.getInt(_indexCaseNo)
			holder._tv_caseno.text = caseNo.toString()
			holder._tv_caseno.tag = caseNo
			holder._ib_map.tag = caseNo
			holder._ib_map.setImageResource(if (caseNo > _caseNo_yellow_max) R.mipmap.mapicon else R.mipmap.mapicon_yellow)
			val gender = _cursor!!.getString(_indexGender)
			holder._tv_gender.text = when(gender) {
				"M" -> getString(R.string.search_male)
				"F" -> getString(R.string.search_female)
				else -> gender
			}
			val age = _cursor!!.getInt(_indexAge)
			holder._tv_age.text = getString(R.string.search_age, age)
			holder._tv_classification.text = _cursor!!.getString(_indexClassification)
		}

	}

}
