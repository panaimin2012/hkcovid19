package p.hkvirusmap

import android.app.Dialog
import android.content.Context
import kotlinx.android.synthetic.main.d_date.*
import p.common.Util

class DDate(context:Context, value:String, private val listener:(String)->Unit) : Dialog(context) {

	init {
		setContentView(R.layout.d_date)
		val v = Util.getDate(value)
		calendar.setDate(v)
		calendar.setOnDateChangeListener { _, year, month, dayOfMonth ->
			dismiss()
			val s_day = if (dayOfMonth > 9) dayOfMonth.toString() else "0$dayOfMonth"
			val m = month + 1
			val s_month = if (m > 9) m.toString() else "0$m"
			val s = "$s_day/$s_month/$year"
			listener.invoke(s)
		}
	}
}