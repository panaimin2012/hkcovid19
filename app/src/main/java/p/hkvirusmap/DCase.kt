package p.hkvirusmap

import android.app.Activity
import android.app.Dialog
import kotlinx.android.synthetic.main.d_case.*
import p.data.DB
import p.data.TblCase

class DCase(context:Activity, caseNo:Int) : Dialog(context) {

	init {
		setContentView(R.layout.d_case)
		tv_caseno.text = caseNo.toString()
		val q = DB.query(TblCase, "${TblCase._caseNo}=$caseNo")
		if (q.moveToNext()) {
			tv_confirmdate.setText(q.getString(q.getColumnIndex(TblCase._confirmDate)))
			tv_onset.setText(q.getString(q.getColumnIndex(TblCase._onsetDate)))
			val gender = q.getString(q.getColumnIndex(TblCase._gender))
			tv_gender.text = when(gender) {
				"M" -> context.getString(R.string.search_male)
				"F" -> context.getString(R.string.search_female)
				else -> gender
			}
			val age = q.getInt(q.getColumnIndex(TblCase._age))
			tv_age.setText( context.getString(R.string.case_age, age))
			tv_hospital.setText(q.getString(q.getColumnIndex(TblCase._hospital)))
			tv_state.setText(q.getString(q.getColumnIndex(TblCase._state)))
			tv_hkresident.setText(q.getString(q.getColumnIndex(TblCase._hkResident)))
			tv_classification.setText(q.getString(q.getColumnIndex(TblCase._classification)))
			tv_confirmed.setText(q.getString(q.getColumnIndex(TblCase._confirmed)))
			tv_district.setText(q.getString(q.getColumnIndex(TblCase._district)))
			tv_building.setText(q.getString(q.getColumnIndex(TblCase._building)))
			tv_related.setText(q.getString(q.getColumnIndex(TblCase._related)))
		}
		q.close()
	}
}