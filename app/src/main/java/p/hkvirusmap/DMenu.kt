package p.hkvirusmap

import android.app.Activity
import android.app.Dialog
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.SimpleAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.lv.*

import java.util.ArrayList

class DMenu(private val _activity: Activity,
            _data:ArrayList<Pair<Int, Int>>,  // image resource id -> text resource id
            private val _listener: (Int) -> Unit) // text resource id
	: Dialog(_activity) {

	private val _internalData = ArrayList<HashMap<String, Any>>()

	private val _itemClicker = AdapterView.OnItemClickListener { _, _, position, _ ->
		dismiss()
		_listener.invoke(_internalData[position][LABEL] as Int)
	}

	init {
		setTitle(R.string.app_name)
		setContentView(R.layout.lv)
		for (p in _data) {
			val item = HashMap<String, Any>()
			item[RESOURCE_ID] = p.first
			item[LABEL] = p.second
			_internalData.add(item)
		}
		lv.adapter = AdpMenu()
		lv.onItemClickListener = _itemClicker
	}

	private inner class AdpMenu
		: SimpleAdapter(_activity, _internalData, R.layout.r_op_menu, arrayOf(RESOURCE_ID, LABEL), intArrayOf(R.id.iv_item, R.id.tv_item)) {
		init {
			viewBinder = ViewBinder { view, data, _ ->
				(view as? ImageView)?.setImageResource(data as Int) ?: (view as? TextView)?.setText(data as Int)
				true
			}
		}
	}

	companion object {
		const val TAG = "DMenu"
		private const val RESOURCE_ID = "RESOURCE_ID"
		private const val LABEL = "LABEL"
	}

}
