package p.hkvirusmap

import android.content.ContentValues
import android.database.sqlite.SQLiteConstraintException
import android.graphics.PointF
import android.os.AsyncTask
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import p.common.LogDog
import p.common.MyPref
import p.data.DB
import p.data.TblCase
import p.data.TblCaseMap
import java.lang.Exception

object Fetcher {

	const val TAG = "Fetcher"

	const val USER_AGENT = "USER_AGENT"
	const val USER_AGENT_MOZILLA = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1"
//	const val URL = "https://services8.arcgis.com/PXQv9PaDJHzt8rp0/arcgis/rest/services/Merge_Buffer_0221_View/FeatureServer/0/query?f=json&where=1%3D1&returnGeometry=true&spatialRel=esriSpatialRelIntersects&geometry=%7B%22xmin%22%3A12677116.649082912%2C%22ymin%22%3A2533664.9069279446%2C%22xmax%22%3A12749578.951897288%2C%22ymax%22%3A2574635.154088815%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D&geometryType=esriGeometryEnvelope&inSR=102100&outFields=*&orderByFields=%E5%80%8B%E6%A1%88%E7%B7%A8%E8%99%9F%20desc&outSR=102100&resultOffset=0&resultRecordCount=2000&cacheHint=false"
	const val URL = "https://services8.arcgis.com/PXQv9PaDJHzt8rp0/arcgis/rest/services/Merge_Display_0227_View/FeatureServer/0/query?f=json&where=(Status%20%3D%20%27Existing%27%20OR%20Status%20%3D%20%27History%27)%20AND%20(Case_no_%20%3C%3E%20-1)&returnGeometry=true&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=152&geometry=%7B%22xmin%22%3A12679985.748184536%2C%22ymin%22%3A2504688.542841416%2C%22xmax%22%3A12758257.265148588%2C%22ymax%22%3A2582960.0598054677%2C%22spatialReference%22%3A%7B%22wkid%22%3A102100%7D%7D&geometryType=esriGeometryEnvelope&inSR=102100&outFields=*&outSR=102100&resultType=tile"

	val JSON_CASE_NO = arrayOf("Case_no_", "個案編號")
	val JSON_CONFIRM_DATE = arrayOf("Date_of_laboratory_confirmation", "實驗室確診報告日期")
	val JSON_ONSET_DATE = arrayOf("Date_of_onset", "發病日期")
	val JSON_GENDER = arrayOf("Gender", "性別")
	val JSON_AGE = arrayOf("Age", "年齡")
	val JSON_HOSPITAL = arrayOf("Name_of_hospital_admitted", "入住醫院名稱")
	val JSON_STATUS = arrayOf("Hospitalised_Discharged_Decease", "住院_出院_死亡")
	val JSON_HKRESIDENT = arrayOf("HK_Non_HK_resident", "香港_非香港居民")
	val JSON_CLASS = arrayOf("Case_classification", "個案分類")
	val JSON_CONFIRMED = arrayOf("Confirmed_Probable", "確定_懷疑")
	val JSON_BUILDING = arrayOf("BuildingName", "地區")
	val JSON_DISTRICT = arrayOf("District", "大廈名單")
	val JSON_RELATED_CASE = arrayOf("Related_confirmed_cases", "相關確診個案")

	private var _httpClient : OkHttpClient? = null

	init {
		_httpClient = OkHttpClient.Builder().build()
	}

	fun doIt(listener:(Int)->Unit) {
		FetchTask(listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
	}

	fun normalizeDate(s:String) : String {
		// sometimes the date is in dd/m/yyyy, sometimes in dd/mm/yyyy
		val e = s.split("/")
		if (e.size != 3) {
			LogDog.e(TAG, "Wrong format date:$s")
			return s
		}
		var ret = ""
		ret += if (e[0].length == 1) "0" else ""
		ret += e[0]
		ret += "/"
		ret += if (e[1].length == 1) "0" else ""
		ret += e[1]
		ret += "/"
		ret += e[2]
		return ret
	}

	private class FetchTask(private val _listener:(Int)->Unit) : AsyncTask<Void, Void, Int>() {

		override fun doInBackground(vararg params: Void?): Int {
			val oldCases = HashSet<Int>()
			val cursor = DB.query(TblCase, "")
			val index_caseno = cursor.getColumnIndex(TblCase._caseNo)
			while (cursor.moveToNext())
				oldCases.add(cursor.getInt(index_caseno))
			cursor.close()
			val cv = ContentValues()
			var newCount = 0
			try {
				val request = Request.Builder().url(URL)
					.addHeader(USER_AGENT, USER_AGENT_MOZILLA)
					.addHeader("Referer", "https://smolandsd.maps.arcgis.com/apps/opsdashboard/index.html")
					.addHeader("Origin", "https://smolandsd.maps.arcgis.com")
					.build()
				val response = _httpClient!!.newCall(request).execute()
				val jsonRoot = JSONObject(String(response.body()!!.bytes(), Charsets.UTF_8))
				val features = jsonRoot.getJSONArray("features")
				val lang = MyPref.getPref(MyPref.PREF_LANG, MyPref.PREF_LANG_ENG)
				for (f in 0 until features.length()) {
					try {
						val a = (features[f] as JSONObject).getJSONObject("attributes")
						cv.clear()
						val caseNo = a.getInt(JSON_CASE_NO[lang])
						if (oldCases.contains(caseNo))
							continue
						cv.put(TblCase._caseNo, caseNo)
						val confirmDate = a.getString(JSON_CONFIRM_DATE[lang])
						cv.put(TblCase._confirmDate, normalizeDate(confirmDate))
						cv.put(TblCase._onsetDate, normalizeDate(a.getString(JSON_ONSET_DATE[lang])))
						LogDog.e(TAG, "new case# $caseNo on $confirmDate")
						cv.put(TblCase._gender, a.getString(JSON_GENDER[lang]))
						try {
							cv.put(TblCase._age, a.getInt(JSON_AGE[lang]))
						} catch (e: Exception) {
							cv.put(TblCase._age, 0)
						}
						cv.put(TblCase._hospital, a.getString(JSON_HOSPITAL[lang]))
						cv.put(TblCase._state, a.getString(JSON_STATUS[lang]))
						cv.put(TblCase._hkResident, a.getString(JSON_HKRESIDENT[lang]))
						cv.put(TblCase._classification, a.getString(JSON_CLASS[lang]))
						cv.put(TblCase._confirmed, a.getString(JSON_CONFIRMED[lang]))
						cv.put(TblCase._district, a.getString(JSON_DISTRICT[lang]))
						cv.put(TblCase._building, a.getString(JSON_BUILDING[lang]))
						cv.put(TblCase._related, a.getString(JSON_RELATED_CASE[lang]))
						try {
							DB.sql().insertOrThrow(TblCase._T, null, cv)
							newCount++
						} catch (e: SQLiteConstraintException) {
							// DB.sql().update(TblCase._T, cv, "${TblCase._caseNo}=$caseNo", null)
						}
						val g = (features[f] as JSONObject).getJSONObject("geometry")
						val x = g.getDouble("x")
						val y = g.getDouble("y")
						val latLon =
							TblCaseMap.mercatorToGeographic(PointF(x.toFloat(), y.toFloat()))
						cv.clear()
						cv.put(TblCaseMap._caseNo, caseNo)
						cv.put(TblCaseMap._index, 1)
						cv.put(TblCaseMap._latitude, latLon.y)
						cv.put(TblCaseMap._longitude, latLon.x)
						try {
							DB.sql().insertOrThrow(TblCaseMap._T, null, cv)
						} catch (e: SQLiteConstraintException) {
							DB.sql().update(
								TblCaseMap._T, cv,
								"${TblCaseMap._caseNo}=$caseNo AND ${TblCaseMap._index}=1", null
							)
						}
					} catch (e:Exception) {

					}
				}
			} catch (e:Exception) {
				LogDog.e(TAG, e.message)
			}
			return newCount
		}

		override fun onPostExecute(newCount: Int) {
			super.onPostExecute(newCount)
			_listener.invoke(newCount)
		}

	}

	// sample json response

	/*
	one sample element
{"features":[
	{"attributes":
		{"ObjectId":2253,
		"Case_no_":556,
		"Date_of_laboratory_confirmation":"28/03/2020",
		"Date_of_onset":"16/03/2020",
		"Gender":"F",
		"Age":20,
		"Name_of_hospital_admitted":"North District Hospital",
		"Hospitalised_Discharged_Decease":"Discharged",
		"HK_Non_HK_resident":"HK resident",
		"Case_classification":"Imported",
		"Confirmed_Probable":"Confirmed",
		"個案編號":556,
		"實驗室確診報告日期":"28/03/2020",
		"發病日期":"16/03/2020",
		"性別":"女",
		"年齡":20,
		"入住醫院名稱":"北區醫院",
		"住院_出院_死亡":"出院",
		"香港_非香港居民":"香港居民",
		"個案分類":"輸入個案",
		"確定_懷疑":"確診",
		"BuildingName":"Grand City Hotel",
		"District":"Central & Western",
		"Related_confirmed_cases":"556",
		"地區":"中西區",
		"大廈名單":"華麗都會酒店",
		"相關確診個案":"556",
		"Status":"Existing",
		"DateoftheLastCase":null,
		"最後有個案在出現病徵期間逗留的日期":null,
		"Status_Chi":"現有"},
	"geometry":{"x":12706135,"y":2546000}
	}
}
	* */

}
