package p.hkvirusmap

import android.app.Application

import p.common.LogDog
import p.common.MyPref
import p.common.Util
import p.data.DB

class PApp : Application() {

	override fun onCreate() {
		super.onCreate()
		// this code snippet checks cursor close
		/* StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
			.detectLeakedSqlLiteObjects()
			.detectLeakedClosableObjects()
			.penaltyLog()
			.penaltyDeath()
			.build()); */
		_instance = this
		// initialize
		LogDog.myinit()
		LogDog.fileMode(true)
		Util.myinit()
		MyPref.myInit()
		DB.myinit()
		//MobileAds.initialize(this, getString(R.string.ad_app_id))
	}

	override fun onTerminate() {
		super.onTerminate()
		LogDog.i(TAG, "PApp terminating...")
		DB.myclose()
		MyPref.myClose()
		Util.myclose()
		LogDog.myclose()
		_instance = null
	}

	companion object {
		const val TAG = "PApp"
		var _instance : PApp? = null
	}

}
