package p.hkvirusmap

import android.app.Dialog
import android.content.Context
import kotlinx.android.synthetic.main.d_lang.*
import p.common.MyPref

class DLang(context:Context, private val _listener:()->Unit) : Dialog(context) {

	init {
		setContentView(R.layout.d_lang)
		setCancelable(false)
		setCanceledOnTouchOutside(false)
		btn_ok.setOnClickListener { okClicked() }
	}

	private fun okClicked() {
		MyPref.setPref(MyPref.PREF_LANG, if (rb_english.isChecked) MyPref.PREF_LANG_ENG else MyPref.PREF_LANG_CHN)
		dismiss()
		_listener.invoke()
	}

}