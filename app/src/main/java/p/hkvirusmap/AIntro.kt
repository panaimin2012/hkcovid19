package p.hkvirusmap

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import p.common.MyPref
import p.common.Util

class AIntro : AppCompatActivity() {

	private val _dlangListener : ()->Unit = {
		checkPermission()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.a_intro)
		supportActionBar?.hide()
		if (MyPref.getPref(MyPref.PREF_LANG, MyPref.PREF_LANG_UNKNOWN) == MyPref.PREF_LANG_UNKNOWN)
			DLang(this, _dlangListener).show()
		else
			checkPermission()
	}

	private fun goAhead() {
		val i = Intent(this, AMap::class.java)
		startActivity(i)
		finish()
	}

	// Permission related

	private val REQUEST_PERMISSION = 101

	private fun checkPermission() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
				Util.showAlert(this, R.string.map_permission_error, {}, null)
				finish()
			} else {
				ActivityCompat.requestPermissions(
					this,
					arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
					REQUEST_PERMISSION
				)
			}
		} else
			goAhead()
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		when (requestCode) {
			REQUEST_PERMISSION -> {
				if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
					Util.showAlert(this, R.string.map_permission_error, {}, null)
					finish()
				} else
					goAhead()
			}
		}
	}

}
