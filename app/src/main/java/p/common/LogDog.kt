package p.common

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.lang.Thread.UncaughtExceptionHandler

import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import android.os.Build
import android.os.Environment.*
import android.os.Looper
import android.util.Log
import p.hkvirusmap.PApp
import p.hkvirusmap.BuildConfig

object LogDog : UncaughtExceptionHandler {

	const val TAG = "Log"

	private var _adb = false
	private var _file = false
	private val _tags = emptyArray<String>()

	fun i(tag: String, message: String?) {
		if (!_tags.isEmpty() && !_tags.contains(tag))
			return
		if (_adb && message != null)
			Log.i(tag, message)
		if (_file) {
			writer?.println(" i:" + tag + ":" + message)
			writer?.flush()
		}
	}

	fun e(tag: String, message: String?) {
		if (_adb && message != null)
			Log.e(tag, message)
		if (_file) {
			writer?.println(" e:" + tag + ":" + message)
			writer?.flush()
		}
	}

	private val PATH = getExternalStorageDirectory().path + "/log"

	private lateinit var _fileName: String
	private lateinit var _packageName: String
	private var _packageManager: PackageManager? = null
	private var _defaultHandler: Thread.UncaughtExceptionHandler? = null
	private var _writer: PrintWriter? = null

	// check if we have SD card
	val writer: PrintWriter?
		get() {
			if (_writer != null)
				return _writer
			if (getExternalStorageState() != MEDIA_MOUNTED) {
				Log.w(TAG, "sdcard unmounted,skip dump exception")
				return null
			}
			val dir = File(PATH)
			if (!dir.exists()) {
				dir.mkdirs()
			}
			val file = File(PATH + "/" + _fileName)
			try {
				_writer = PrintWriter(BufferedWriter(FileWriter(file, true)))
			} catch (e: IOException) {
				Log.e(TAG, "Failed to open $PATH/$_fileName")
				_writer = null
			}
			return _writer
		}

	override fun uncaughtException(thread: Thread, ex: Throwable) {
		// first display a toast message
		object : Thread() {
			override fun run() {
				Looper.prepare()
			}
		}.start()
		try {
			dump(ex)
		} catch (e: Exception) {
			Log.e(TAG, "Failed to dump because " + e.message)
			e.printStackTrace()
		}

		if (_defaultHandler != null) {
			_defaultHandler!!.uncaughtException(thread, ex)
		} else {
			// sleep so the toast can have time to display
			try {
				Thread.sleep(3000)
			} catch (e: InterruptedException) {
			}

			android.os.Process.killProcess(android.os.Process.myPid())
		}
	}

	@Throws(IOException::class, NameNotFoundException::class)
	private fun dump(ex: Throwable) {
		val writer1 = writer ?: return
		val now = System.currentTimeMillis()
		writer1.println(now)
		val pi = _packageManager!!.getPackageInfo(_packageName, PackageManager.GET_ACTIVITIES)
		writer1.print("App Version: ")
		writer1.print(pi.versionName)
		writer1.print('_')
		writer1.print("OS Version: ")
		writer1.print(Build.VERSION.RELEASE)
		writer1.print("_")
		writer1.println(Build.VERSION.SDK_INT)
		writer1.print("Vendor: ")
		writer1.println(Build.MANUFACTURER)
		writer1.print("Model: ")
		writer1.println(Build.MODEL)
		writer1.print("AppVersion:")
		writer1.println(BuildConfig.VERSION_CODE)
		writer1.println()
		ex.printStackTrace(writer)
		writer1.flush()
		myclose()
		val file = File("$PATH/$_fileName")
		val file2 = File("$PATH/$_fileName.$now.txt")
		file.renameTo(file2)
	}

	fun myinit() {
		_packageName = PApp._instance!!.packageName
		_packageManager = PApp._instance!!.packageManager
		val inEmulator = Build.FINGERPRINT.contains("generic")
		_adb = inEmulator
		fileMode(false)
		_defaultHandler = Thread.getDefaultUncaughtExceptionHandler()
		Thread.setDefaultUncaughtExceptionHandler(this)
	}

	fun fileMode(file: Boolean) {
		_file = file
		if (_file)
			_fileName = "$_packageName.txt"
	}

	fun myclose() {
		_writer?.close()
		_writer = null
		Thread.setDefaultUncaughtExceptionHandler(_defaultHandler)
		_defaultHandler = null
		_packageManager = null
	}

}
