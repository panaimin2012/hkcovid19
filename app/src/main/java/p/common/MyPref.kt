package p.common

import android.content.SharedPreferences
import android.preference.PreferenceManager
import p.hkvirusmap.PApp

object MyPref {

	const val PREF_LANG = "PREF_LANG"
	const val PREF_LANG_UNKNOWN = 99
	const val PREF_LANG_ENG = 0
	const val PREF_LANG_CHN = 1

	const val PREF_LAST_UPDATE = "PREF_LAST_UPDATE"

	const val PREF_LAST_QUERY_DATE = "PREF_LAST_QUERY_DATE"

	private var sp_: SharedPreferences? = null

	fun myInit() {
		sp_ = PreferenceManager.getDefaultSharedPreferences(PApp._instance)
	}

	fun myClose() {
		sp_ = null
	}

	fun getPref(key: String, def: Long): Long {
		return sp_!!.getLong(key, def)
	}

	fun getPref(key: String, def: String): String {
		return sp_!!.getString(key, def)!!
	}

	fun getPref(key: String, def: Int): Int {
		return sp_!!.getInt(key, def)
	}

	fun getPref(key: String, def: Boolean): Boolean {
		return sp_!!.getBoolean(key, def)
	}

	fun setPref(key: String, value: String) {
		val editor = sp_!!.edit()
		editor.putString(key, value)
		editor.apply()
	}

	fun setPref(key: String, value: Int) {
		val editor = sp_!!.edit()
		editor.putInt(key, value)
		editor.apply()
	}

	fun setPref(key: String, value: Long) {
		val editor = sp_!!.edit()
		editor.putLong(key, value)
		editor.apply()
	}

	fun setPref(key: String, value: Boolean) {
		val editor = sp_!!.edit()
		editor.putBoolean(key, value)
		editor.apply()
	}

}
