package p.common

import android.app.Activity
import android.app.AlertDialog
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.widget.Toast

import java.text.SimpleDateFormat

import p.hkvirusmap.PApp

object Util {

	private const val TAG = "Util"

	val MILLISECONDS_PER_HOUR = 3600000L
	val SIX_HOUR = MILLISECONDS_PER_HOUR * 6
	val MILLISECONDS_PER_DAY = MILLISECONDS_PER_HOUR * 24

	private var _toast: Toast? = null
	private var _sp: SharedPreferences? = null

	private val _fmt = SimpleDateFormat("dd/MM/yyyy")

	fun myinit() {
		_sp = PreferenceManager.getDefaultSharedPreferences(PApp._instance)
	}

	fun myclose() {
		_toast = null
		_sp = null
	}

	fun getDate(s: String): Long {
		try {
			return _fmt.parse(s).time
		} catch (ignored: Exception) {		}
		LogDog.e("Util", "Parse date error:$s")
		return 0
	}

	fun getDateString(v:Long) : String {
		return _fmt.format(v)
	}

	fun showToast(s: String?) {
		if (_toast != null)
			_toast!!.cancel()
		_toast = Toast.makeText(PApp._instance, s, Toast.LENGTH_SHORT)
		_toast!!.show()
		LogDog.e("Util", s)
	}

	fun showToast(resId: Int) {
		showToast(PApp._instance!!.resources.getString(resId))
	}

	fun showAlert(activity: Activity, s : String, callbackYes : ()->Unit, callbackNo : (()->Unit)?) {
		var builder = AlertDialog.Builder(activity)
				.setMessage(s)
				.setPositiveButton("OK") { _, _ -> callbackYes.invoke() }
		if (callbackNo != null)
			builder = builder.setNegativeButton("NO") { _, _->callbackNo.invoke() }
		val dlg = builder.create()
		dlg.show()
	}

	fun showAlert(activity: Activity, resId : Int, callbackYes: ()->Unit, callbackNo: (() -> Unit)?) {
		showAlert(activity, activity.getString(resId), callbackYes, callbackNo)
	}

}
