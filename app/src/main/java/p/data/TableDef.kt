package p.data

open class TableDef {

	lateinit var _T: String
	lateinit var _COLUMN_MAP: Map<String, Int>
	var _UNIQUE_INDEXES: Array<Array<String>>? = null
	var _OTHER_INDEXES: Array<Array<String>>? = null

	companion object {

		val TYPE_INT = 1
		val TYPE_TEXT = 2
		val TYPE_DATETIME = 3
		val TYPE_REAL = 4

		val _ID = "_id"
	}

}