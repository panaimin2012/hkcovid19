package p.data

import android.graphics.PointF

object TblCaseMap : TableDef() {

	const val _caseNo = "_caseNo"
	const val _index = "_index"
	const val _latitude = "_latitude"
	const val _longitude = "_longitude"

	init {
		_T = "CaseMap"
		_COLUMN_MAP = mapOf(
			_caseNo to TYPE_INT,
			_index to TYPE_INT,
			_latitude to TYPE_REAL,
			_longitude to TYPE_REAL )
		_UNIQUE_INDEXES = arrayOf( arrayOf(_caseNo, _index) )
		_OTHER_INDEXES = arrayOf( arrayOf(_latitude, _longitude))
	}

	fun mercatorToGeographic(p : PointF) : PointF {
		val mercatorX_lon = p.x
		val mercatorY_lat = p.y
		if (Math.abs(mercatorX_lon) < 180 && Math.abs(mercatorY_lat) < 90)
			return p
		if ((Math.abs(mercatorX_lon) > 20037508.3427892) || (Math.abs(mercatorY_lat) > 20037508.3427892))
			return p
		val x = mercatorX_lon
		val y = mercatorY_lat
		val num3 = x / 6378137.0
		val num4 = num3 * 57.295779513082323
		val num5 = Math.floor(((num4 + 180.0) / 360.0))
		val num6 = num4 - (num5 * 360.0)
		val num7 = 1.5707963267948966 - (2.0 * Math.atan(Math.exp((-1.0 * y) / 6378137.0)))
		val lon = num6
		val lat = num7 * 57.295779513082323
		return PointF(lon.toFloat(), lat.toFloat())
	}

	fun geographicToMercator(p: PointF) : PointF {
		val mercatorX_lon = p.x
		val mercatorY_lat = p.y
		if ((Math.abs(mercatorX_lon) > 180 || Math.abs(mercatorY_lat) > 90))
			return p

		val num = mercatorX_lon * 0.017453292519943295
		val x = 6378137.0 * num
		val a = mercatorY_lat * 0.017453292519943295;

		val lon = x
		val lat = 3189068.5 * Math.log((1.0 + Math.sin(a)) / (1.0 - Math.sin(a)))
		return PointF(lon.toFloat(), lat.toFloat())
	}

}