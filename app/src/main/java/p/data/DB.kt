package p.data

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

import p.common.LogDog
import p.hkvirusmap.PApp

object DB {

	fun myinit() {
		if (_implementation != null)
			return
		_implementation = Imple()
		_implementation!!.readableDatabase
	}

	fun sql() : SQLiteDatabase = _implementation!!.writableDatabase

	fun myclose() {
		LogDog.i(TAG, "Closing database")
		_implementation?.close()
		_implementation = null
	}

	fun query(db: SQLiteDatabase, t: TableDef, where: String?, orderBy: String? = null): Cursor {
		val defColumns = t._COLUMN_MAP.keys.toTypedArray()
		val queryColumns = arrayOfNulls<String>(defColumns.size + 1)
		queryColumns[0] = TableDef._ID
		System.arraycopy(defColumns, 0, queryColumns, 1, defColumns.size)
		return db.query(t._T, queryColumns, where, null, null, null, orderBy)
	}

	fun query(t: TableDef, where: String?, orderBy: String? = null): Cursor {
		return query(_implementation!!.readableDatabase, t, where, orderBy)
	}

	fun getIntMinus1(t: TableDef, column: String, where: String): Int {
		val cols = arrayOf(column)
		val c = _implementation!!.readableDatabase.query(t._T, cols, where, null, null, null, null)
		var ret = -1
		if (c.moveToNext())
			ret = c.getInt(0)
		c.close()
		return ret
	}

	fun getIntMinus1(t: TableDef, column: String, id: Long): Int {
		val where = TableDef._ID + "=" + id
		return getIntMinus1(t, column, where)
	}

	fun getLongMinus1(t: TableDef, column: String, where: String): Long {
		val cols = arrayOf(column)
		val c = _implementation!!.readableDatabase.query(t._T, cols, where, null, null, null, null)
		var ret = -1L
		if (c.moveToNext())
			ret = c.getLong(0)
		c.close()
		return ret
	}

	fun getLongMinus1(t: TableDef, column: String, id: Long): Long {
		val where = TableDef._ID + "=" + id
		return getLongMinus1(t, column, where)
	}

	fun getString(t: TableDef, column: String, where: String?): String? {
		val cols = arrayOf(column)
		val c = _implementation!!.readableDatabase.query(t._T, cols, where, null, null, null, null)
		var ret: String? = null
		if (c.moveToNext())
			ret = c.getString(0)
		c.close()
		return ret
	}

	fun getString(t: TableDef, column: String, id: Long): String? {
		val where = TableDef._ID + "=" + id
		return getString(t, column, where)
	}

	private val TAG = "DB"
	private val DBNAME = "hkvirusmap.db"
	private val VERSION = 3

	private val _tables =  arrayOf( TblCase, TblCaseMap )

	private class Imple : SQLiteOpenHelper(PApp._instance, DBNAME, null, VERSION) {

		override fun onCreate(db: SQLiteDatabase) {
			createTables(db)
		}

		override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
			createTables(db)
		}

		private fun createTables(db: SQLiteDatabase) {
			val sql = StringBuilder()
			for (t in _tables) {
				sql.delete(0, sql.length)
				sql.append("CREATE TABLE ").append(t._T).append(" ( ")
				sql.append(TableDef._ID).append(" INTEGER PRIMARY KEY ")
				val columns = t._COLUMN_MAP.keys.toList()
				for (i in 0 until columns.size) {
					sql.append(", ").append(columns[i])
					val columnType = t._COLUMN_MAP[columns[i]]
					sql.append(if (columnType == TableDef.TYPE_INT || columnType == TableDef.TYPE_DATETIME)
						" INTEGER "
					else if (columnType == TableDef.TYPE_REAL)
						" REAL "
					else
						" TEXT ")
				}
				sql.append(" ) ")
				try {
					db.execSQL(sql.toString())
				} catch (e: Exception) {
					LogDog.e(TAG, "Error run sql:" + e.message + ", sql:" + sql)
				}

			}
			createIndexes(db)
		}

		private fun createIndexes(db: SQLiteDatabase) {
			val sql = StringBuilder()
			for (t in _tables) {
				if (t._UNIQUE_INDEXES != null) {
					for (j in 0..t._UNIQUE_INDEXES!!.size - 1) {
						val oneIndex = t._UNIQUE_INDEXES!![j]
						sql.delete(0, sql.length)
						sql.append("CREATE UNIQUE INDEX ").append(t._T).append("_ux_$j ON ").append(t._T).append("(")
						for (i in oneIndex.indices) {
							if (i > 0)
								sql.append(", ")
							sql.append(oneIndex[i])
						}
						sql.append(")")
						try {
							db.execSQL(sql.toString())
						} catch (e: Exception) {
							LogDog.e(TAG, "Error run sql:" + e.message + ", sql:" + sql)
						}

					}
				}
				if (t._OTHER_INDEXES != null) {
					for (j in 0..t._OTHER_INDEXES!!.size - 1) {
						val oneIndex = t._OTHER_INDEXES!![j]
						sql.delete(0, sql.length)
						sql.append("CREATE INDEX ").append(t._T).append("_x_$j ON ").append(t._T).append("(")
						for (i in oneIndex.indices) {
							if (i > 0)
								sql.append(", ")
							sql.append(oneIndex[i])
						}
						sql.append(")")
						try {
							db.execSQL(sql.toString())
						} catch (e: Exception) {
							LogDog.e(TAG, "Error run sql:" + e.message + ", sql:" + sql)
						}

					}
				}
			}
		}

	}

	private var _implementation : Imple? = null

}
