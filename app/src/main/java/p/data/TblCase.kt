package p.data

object TblCase : TableDef() {

	const val _caseNo = "_caseNo"
	const val _confirmDate = "_confirmDate"
	const val _onsetDate = "_onsetDate"
	const val _gender = "_gender"
	const val _age = "_age"
	const val _hospital = "_hospital"
	const val _state = "_state"         // hospitalized, discharged, decease
	const val _hkResident = "_hkResident"
	const val _classification = "_classification"   // imported, local
	const val _confirmed = "_confirmed"
	const val _building = "_building"
	const val _district = "_district"
	const val _related = "_related"

	init {
		_T = "TCase"
		_COLUMN_MAP = mapOf(
			_caseNo to TYPE_INT,
			_confirmDate to TYPE_TEXT,
			_onsetDate to TYPE_TEXT,
			_gender to TYPE_TEXT,
			_age to TYPE_INT,
			_hospital to TYPE_TEXT,
			_state to TYPE_TEXT,
			_hkResident to TYPE_TEXT,
			_classification to TYPE_TEXT,
			_confirmed to TYPE_TEXT,
			_building to TYPE_TEXT,
			_district to TYPE_TEXT,
			_related to TYPE_TEXT
		)
		_UNIQUE_INDEXES = arrayOf( arrayOf(_caseNo) )
	}

}